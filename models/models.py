from odoo import models, fields, api
from odoo.exceptions import UserError

from fhirclient import client

class fhir_endpoint(models.Model):
  _name = 'fhir_config.config'

  key = fields.Char(string='Key')
  value = fields.Char(string='Value')

  _sql_constraints = [('key', 'unique(key)', 'A key already exists.')]

  def getEndpoint(self):
    endpoint = self.search([('key', '=', 'endpoint')]);
    
    if not endpoint:
      raise UserError('FHIR integration not configured properly, missing endpoint configuration.')
    
    return endpoint.value

  def getClientInstance(self):
    appId = 'admin-ehealth'
    endpoint = self.getEndpoint()

    return client.FHIRClient(settings={
      "app_id": appId,
      "api_base": endpoint
    })
