# -*- coding: utf-8 -*-
{
    'name': "FHIR Configuration",

    'summary': """
        FHIR Integration module""",

    'description': """
         FHIR Integraton module.
    """,

    'author': "eHealth ID",
    'website': "https://ehealth.id/",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base'],

    'data': [
        'views/fhir_config.xml',
        'data/default.xml',
    ],
}
